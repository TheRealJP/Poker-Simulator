
class Player(object):
    def __init__(self, username: str):
        self.username = username
        ''' The name of this player. '''

        self.hand = [Card]
        ''' A list of cards (2) representing this player's hand.'''

    def fold(self):
        ''' '''
        self.hand = [Card]

class Card(object):
	def __init__(self, suit, value):
		self.suit = suit
		self.value = value

	def __cmp__(self, other):
			if self.value < other.value:
				return -1
			elif self.value == other.value:
				return 0
			return 1
